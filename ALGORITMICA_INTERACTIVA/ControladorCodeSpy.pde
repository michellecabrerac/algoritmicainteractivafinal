class ControllerCodeSpy {
  ModelCodeSpy modelCodeSpy;
  ViewCodeSpy viewCodeSpy;
  HashMap<String, ArrayList> linesStores;

  
  public ControllerCodeSpy(ModelCodeSpy modelCodeSpy, ViewCodeSpy viewCodeSpy){
    this.modelCodeSpy = modelCodeSpy;
    this.viewCodeSpy = viewCodeSpy;
    getMain();
    setLinePositions();
    sendLinesToPrint(); // send codeLineToprint
  }

  void printAll(){
    getValuesToPrint(); // pide Al modelo que te envie los datos de los valores para mostrarlos
    sendValueslinesToPrint(); // se lo pasa a la vista
    viewCodeSpy.setWhichLineIsExecuting(getWhichLineIsExecuting());
    sendLinesToPrint(); // code lines 
    viewCodeSpy.printValuesPresentation();/*englobarla en printall*/
    viewCodeSpy.printArrays();
    if(thereAreException == true || compilationException == true){
      viewCodeSpy.setMessageException(messageException, width/2+150, 50); // si hay una exception pintala!!!!
    }
    if(!compilationAvaible){
      viewCodeSpy.setMessageException(modelCodeSpy.getMessageNoCompilationAvaible(),width/2+150,height-50); // Si la compilación no se ha podido comprobar informa al usuario
    }

  }
  void sendLinesToPrint(){
    int beginOfLine = 0;
    if(!getWhoIsExecutingNow().equals("main")){
      beginOfLine = 1;
    }
    viewCodeSpy.setLineToPrint(getCodeLines(), beginOfLine); // like arraylist   
    viewCodeSpy.setLineToPrintArray(getCodeLineArray(), beginOfLine);  //like String[]
  }
  ArrayList getCodeLines(){
    return linesStores.get(getWhoIsExecutingNow());
  }
  String [] getCodeLineArray(){
    ArrayList<String> codeLines = linesStores.get(getWhoIsExecutingNow());
    String [] codeLinesArray = new String [codeLines.size()];
    for(int i = 0; i<codeLinesArray.length; i++){
      codeLinesArray[i] = codeLines.get(i);
    }
    return codeLinesArray;
  }
  int getWhichLineIsExecuting(){ // OJO CON ESTO DEBE DE ESTAR BIIIEN SINCRONIZADOO!!
    return modelCodeSpy.getIdExecuteLine();
  }
  String getWhoIsExecutingNow(){ // para saber quien se esta ejecutando. Para pasarle a la vista las líneas correctas
    return modelCodeSpy.getWhoIsExecutingNow();
  }
  void setModelAndView(ModelCodeSpy modelCodeSpy, ViewCodeSpy viewCodeSpy) {
    this.modelCodeSpy = modelCodeSpy;
    this.viewCodeSpy = viewCodeSpy;
  }
  private void getMain(){
    linesStores = modelCodeSpy.getLinesStores();
     viewCodeSpy.setLineToPrint(linesStores.get("main"),0); // beginOfLine = 0 porque es el main
  }
  private void sendValueslinesToPrint(){ // send?
    String[] valueslinesToPrint = getValuesToPrint();
    if(valueslinesToPrint != null){
       viewCodeSpy.setValueslinesToPrint(valueslinesToPrint);    
    }
    viewCodeSpy.setArrayValueslinesToPrint(getArrayValuesToPrint()); // para la animación de los arrays
    viewCodeSpy.setIsPrimitiveArray(getIsPrimitiveArray()); // para la animación de los arrays
  }
  HashMap <String ,Object []> getArrayValuesToPrint(){
    HashMap <String ,Object []> object = modelCodeSpy.getArrayValuesLines();
    return object;
  }
    HashMap <String,Boolean> getIsPrimitiveArray(){
      HashMap <String ,Boolean> object = modelCodeSpy.getIsPrimitiveArray();
      return object;
  }
    String[] getValuesToPrint(){
    return  modelCodeSpy.getValuesToPrint();
  }
  private void setLinePositions(){
    int size = linesStores.get(getWhoIsExecutingNow()).size(); 
    /*coge el lineStore correspondiente a lo que se esta ejecutando... para poder definir sus posiciones */
    int[][] positions = new int [size][2];
    int x = 100;
    int y = 150;
    int espaciado = 50;
    for(int i = 0; i<positions.length; i++){ // asignación de valores
      positions[i][0] = x;
      positions[i][1] = y;
      y = y + espaciado;
    }
    viewCodeSpy.setLinePositions(positions);
  } 
   void checkPushButtonNext(int x, int y){
     boolean pushTheNextButton = viewCodeSpy.checkPushButtonNext(x,y);
     if(pushTheNextButton){
       modelCodeSpy.next();
     }
  }
  void pushNextButton(){ // Cuando pican next desde el editoor
    modelCodeSpy.next(); // le decimos next al modelo
    printAll();
    viewCodeSpy.setWhichLineIsExecuting(getWhichLineIsExecuting());
    viewCodeSpy.setLinesInTextArea();
    viewCodeSpy.pushNextButton();
  }
  void pushCompileButton(){
    viewCodeSpy.pushCompileButton();
    try{
       modelCodeSpy.crearClassFile();
       compilationAvaible = modelCodeSpy.getCompilationAvaible();
       compilationException = modelCodeSpy.getCompilateException();
       checkCompilateException();
    }catch(Exception ex){
        compilationException = true;
    }

    //Si le das a compile el modelo tiene que ser todo nuevo no?
    //modelCodeSpy = new ModelCodeSpy(); 
    printAll();
  }
  void pushEditButton(){
    modelCodeSpy.setCompilationException(false);
    viewCodeSpy.pushEditButton();
    String text = modelCodeSpy.getAllCodeToExaminateEDIT();
    viewCodeSpy.editTextarea(text);
  }
  void pushSaveButton(){
    viewCodeSpy.pushSaveButton();
    String [] textTextarea = viewCodeSpy.getTextTextarea();
    modelCodeSpy.saveCodeToExaminateSAVE(textTextarea); //coge lo que tengas en el textArea y guardalo en el txt global
    modelCodeSpy.readAndSeparateFiles(); // separa el txt grande en los individuales
  }
  boolean thereAreException = false;
  String messageException= "";
  boolean compilationException = false;
  boolean compilationAvaible = true;
  String getMessageException(){
    return messageException;
  }
  /* To get Values of atributes*/
  void executeMainMethodOfExaminatedClass(){
    try{ /*LO PONEMOS DENTRO DE UN TRY CATCH PORQUE SI HA HABIDO UN ERROR EN TIEMPO DE EJECUCIÓN LO PODAMOS CONTROLAR*/
      modelCodeSpy.executeMainMethodOfExaminatedClass();
    }catch(Exception ex){
        thereAreException =  true;
        messageException = "Excepción en tiempo de ejecución: "+ ex;
        //viewCodeSpy.setMessageException(messageException);
    }
    
  }
  boolean thereAreCompilationException(){
    return compilationException;
  }
  void checkCompilateException(){
    if(compilationException){
        messageException = "Excepción en tiempo de compilacion";
        //viewCodeSpy.setMessageException(messageException); 
    }
  }
  
  /*FOR TEXT AREA*/
  void showTextArea(GTextArea teaxtArea, GButton nextButton, GButton compileButton){
    viewCodeSpy.showTextArea(teaxtArea,nextButton,compileButton);
  }
  
}