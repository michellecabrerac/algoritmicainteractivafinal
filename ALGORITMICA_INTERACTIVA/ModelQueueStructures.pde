import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.*;
import java.util.AbstractQueue;
class ModelQueueStructures implements ModelDataStructures{
  Queue queue;
  String typeOfQueue;
  public ModelQueueStructures(String type){
    setTypeOfQueue(type);
  }
  private void setTypeOfQueue(String type){
    String classType = type.toUpperCase();
      switch(classType){
      case "ARRAYBLOCKINGQUEUE":
        typeOfQueue = "ArrayBlockingQueue";
        queue =  new ArrayBlockingQueue(5);
      break;
      case "ARRAYDEQUE":
         typeOfQueue = "ArrayDeque";
         queue =  new ArrayDeque();
      break;
      default: 
      typeOfQueue = "ArrayBlockingQueue";
      queue =  new ArrayBlockingQueue(5);
    }
  }
  public String getClassType(){
    return this.typeOfQueue;
  }
  String performAction(String action){
    String result = "";
      switch(action){
      case "add":
      result = result + queue.add(new Object());
      break;
      case "remove":
       result = result + queue.remove();
      break;
      case "element":
       result = result + queue.element();
      break;
      case "offer":
        result = result + queue.offer(new Object());
      break;
      case "poll":
       result = result + queue.poll();
      break;
      case "peek":
       result = result +  queue.peek();
      break;
      case "clear":
       queue.clear();
      break;
    }
    return result;
  }
  int getSize(){
    return queue.size();
  }
    String[] getObjectsIDs(){
    Object [] array = queue.toArray();
    String [] ids = new String [array.length];
    for(int i = 0; i<array.length; i++){
      ids[i] = array[i].toString();
    }
    return ids;
  }
  
}