class ControllerDataStructures {
  ModelDataStructures modelStructures;
  ViewDataStructures viewStructures;
  void setModelAndView(ModelDataStructures modelStructures, ViewDataStructures viewStructures) {
    this.modelStructures = modelStructures;
    this.viewStructures = viewStructures;
  }
  void createStructure(String structure){
    switch(structure){
      case "QUEUE":
        setModelAndView(new ModelQueueStructures("ArrayBlockingQueue"), new ViewStructuresQueue());
      break;
      case "STACK":
      setModelAndView(new ModelStackStructures("Stack"), new ViewStructuresStack());
      break;
      case "LIST":
      setModelAndView(new ModelListStructures("ArrayList"), new ViewStructuresList());
      break;
    }
  }
  void buttonPressed (int x, int y){ // Alguien a tocado un botón
  if(viewStructures != null && modelStructures != null){
    String actionPressed = viewStructures.getButtonPressed(x,y); // mira que botón a sido
    performedMethod(actionPressed); // ejecuta la acción tanto para el modelo como para la vista
  }
  }
  void printStructuresAnimation(){
    if(viewStructures != null){
      viewStructures.drawStructure();
    }
  }
    void performedMethod(String action){
    try{
       String result = modelStructures.performAction(action);
       viewStructures.performAction(action, result);
       viewStructures.setClassType(modelStructures.getClassType());
       viewStructures.setSize(modelStructures.getSize());
       viewStructures.setIdsObjects(modelStructures.getObjectsIDs());
    }
    catch(Exception ex){
      viewStructures.performAction("Exception", "Exception: " +ex.getMessage());
    }

  }
}