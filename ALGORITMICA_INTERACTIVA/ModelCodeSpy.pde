import java.util.*; //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>//
import java.lang.reflect.*;

class ModelCodeSpy {
  HashMap<String, ArrayList> linesStores = new HashMap<String, ArrayList>(); // Estarán las lineas correspondientes a el "main" y los otros métodos mapa-> de ArrayList
  // HashMap<String, String[][]> words = new HashMap<String, String[][] >(); // linea separada en palabras
  public ModelCodeSpy() {
    try {    
      this.prepareReflection(); 
      prepareLines(); // para rellenar el hasmap de Arraylist de Stings linesStores que contienes las lineas de código
      //thread("executeMainMethodOfExaminatedClass"); // iNICIAMOS  UN THREAD Y LA EJECUCIÓN
    }
    catch(Exception ex) {
      println("ModelCodeSpy: ", ex.getMessage());
    }
  }

  public void executeMainMethodOfExaminatedClass() {
    objetoClassIWantExaminate.ejecutar();
  }
  HashMap getLinesStores() { // devolvera las lineas que se deben de presentar por pantalla
    return linesStores;
  }
  void prepareLines() { // TO inti all codesLines
    linesStores = getAllUserMethods();
    linesStores.put("main", getMain());
  }
  /* HashMap<String,String[][]> getWords(){
   return words;
   }*/
  void splitWordsFromCodeLine() {
    //palabras = sentencia.split(" ");
  }

  HashMap getAllUserMethods() { // Te devuelve una matriz de Strings 
    /*---*/
    String lines[] = null;
    HashMap methods = new HashMap();
    ArrayList listaDelineas = new ArrayList();
    lines = loadStrings("methods.txt");
    String idMethod = "";
    for (int i = 0; i < lines.length; i++) {
      if (lines[i].trim() != "") { // si viene un espacio en blanco no entres
        if (lines[i].contains("&--")) {
          String[] cadena1 = lines[i].split("&--");
          String cadena2 = cadena1[1];
          String[] cadena3 = cadena2.split("--&");
          idMethod = cadena3[0];
          listaDelineas = new ArrayList();
          listaDelineas.add(lines[i]); //
          methods.put(idMethod, listaDelineas);
        } else {
          listaDelineas.add(lines[i]);
        }
      }
    }
    return methods;
  }

  String getGlobalsVariable() {
    String lines[] = loadStrings("globalsVars.txt");
    String variables = "";
    for (String linea : lines) {
      variables = variables +" "+ linea;
    }
    return variables;
  }
  String getImports() {
    String lines[] = loadStrings("imports.txt");
    String imports = "";
    for (String linea : lines) {
      imports = imports +" \n "+ linea;
    }
    return imports;
  }
  ArrayList getMain() { // Te devuelve una lista de Strings 
    String lines[] = null;
    ArrayList listaDelineas = new ArrayList();
    lines = loadStrings("codigo.txt");
    for (int i = 0; i < lines.length; i++) {
      if (!lines[i].trim().equals("")) {
        listaDelineas.add(lines[i]);
      }
    }
    return listaDelineas;
  }
  //ArrayList<String> valuesLines = new ArrayList();
  String [] valuesLines = new String[0]; // para los valores que se pueden presentar en forma de String
  HashMap <String, Object [] > arrayValuesLines = new HashMap(); //  valores que deben de ser presentados en forma de Arrays
  HashMap <String, Boolean > isPrimitiveArray = new HashMap(); //  valores que deben de ser presentados en forma de Arrays

  /*TO GET VALUES ATTRIBUTES*/
  String[] getValuesToPrint() {
    useReflection();
    return valuesLines;
  }
  HashMap <String, Object [] > getArrayValuesLines() {
    return arrayValuesLines;
  }
  HashMap <String, Boolean > getIsPrimitiveArray() { // para controlar la animación de los Arrays, dado que si es primitivo en 
  //la inicialización actua diferente que si es un número de tipo objeto
  return isPrimitiveArray;
}
  Field[] allTheDeclaredVariables; // atributos
  ClassIWantExaminate objetoClassIWantExaminate;
  void next() {
    objetoClassIWantExaminate.wakeUp(); // wakeUp
    try {
      Thread.sleep(110); //hacemos esperar al principal para que nos diga bien el resultado
    } 
    catch (InterruptedException e) {
      println("Exception: ", e.getMessage());
    }
  }
  int getIdExecuteLine() {
    if (objetoClassIWantExaminate != null) {
      Integer idExecutedLine = objetoClassIWantExaminate.getIdExecuteLine();
      if (idExecutedLine == null) {
        return -1;
      }
      return idExecutedLine;
    }
    return -1;
  }
  String getWhoIsExecutingNow() { // Quien se está ejecutando.. el main? el metodo1 ?
    return objetoClassIWantExaminate.getWhoIsExecutingNow();
  }
  int getLengthAllDeclaredFields() {
    return allTheDeclaredVariables.length;
  }
  void prepareReflection() throws ClassNotFoundException {
    objetoClassIWantExaminate = new ClassIWantExaminate();
    Class c = Class.forName("ClassIWantExaminate");
    allTheDeclaredVariables = c.getDeclaredFields();
    valuesLines = new String[allTheDeclaredVariables.length];
    useReflection(); /*mmm*/
  }
  void useReflection() {
    try {
      for (int k = 0; k<getLengthAllDeclaredFields(); k++) {
        allTheDeclaredVariables[k].setAccessible(true); //
        if (!isAReservedTFGAttribute(allTheDeclaredVariables[k])) {
          allTheDeclaredVariables[k].setAccessible(true); 
          Type type = allTheDeclaredVariables[k].getGenericType();
          String nombreTipoVar = type.getTypeName();
          String tipo = type.toString();
          if (tipo.indexOf("[Ljava.lang.") != -1) { // si es un array de objetos
            tipo = "array";
          }
          if (tipo.indexOf("class [") != -1) { // si es un array de primitivos
            tipo = "primitiveArray";
          }
          switch(tipo) { 
          case "array": 
            allTheDeclaredVariables[k].setAccessible(true);  
            //valuesLines[k] = allTheDeclaredVariables[k].getName()+ ": " +Arrays.deepToString((Object[])allTheDeclaredVariables[k].get(objetoClassIWantExaminate));
            arrayValuesLines.put( allTheDeclaredVariables[k].getName(), (Object[])allTheDeclaredVariables[k].get(objetoClassIWantExaminate) );
            isPrimitiveArray.put( allTheDeclaredVariables[k].getName(), false);
            break;
          case "primitiveArray":
            Object [] newArray = getArrayConverted(nombreTipoVar, allTheDeclaredVariables[k].get(objetoClassIWantExaminate));
            arrayValuesLines.put( allTheDeclaredVariables[k].getName(), newArray);
            isPrimitiveArray.put( allTheDeclaredVariables[k].getName(), true);
            break;
          default: 
            allTheDeclaredVariables[k].setAccessible(true); 
            valuesLines[k]=""+ allTheDeclaredVariables[k].getName()+ ": "+allTheDeclaredVariables[k].get(objetoClassIWantExaminate);  
            break;
          }
        }//FI IF
      }
    }
    catch(Exception ex) {
      println("Exception de access: ", ex);
    }
  }
  private Object[] getArrayConverted(String type, Object oldArray) {
    Object[] newArray = new Object[0]; 
    int size = 0;
    switch(type) {
    case"int[]":
      size = ((int[])oldArray).length;
      newArray = new Object[size];
      for (int i=0; i<size; i++) {
        newArray[i] = ((int[])oldArray)[i];
      }
      break;
    case"float[]":
      size = ((float[])oldArray).length;   
      newArray = new Object[size];
      for (int i=0; i<size; i++) {
        newArray[i] = ((float[])oldArray)[i];
      }
      break;
    case"double[]":
      size = ((double[])oldArray).length;
      newArray = new Object[size];
      for (int i=0; i<size; i++) {
        newArray[i] = ((double[])oldArray)[i];
      }
      break;
    case"long[]":
      size = ((long[])oldArray).length;
      newArray = new Object[size];
      for (int i=0; i<size; i++) {
        newArray[i] = ((long[])oldArray)[i];
      }
      break;
    }
    return newArray;
  }
  private boolean isAReservedTFGAttribute(Field declaredVariables) {
    String attribute =  declaredVariables.getName();
    if (attribute.equals( "TFGnextTFG") || attribute.equals( "TFGidExecuteLineTFG")
    || attribute.equals("TFGwhichOneTFG") || attribute.equals("TFGwhoIsExecutingNowTFG")  ) { 
      return true;
    }
    return false;
  }

  /*TOKENS TO CREATE CLASSIWANTEXECUTE*/
  public void saveCodeToExaminateSAVE(String [] text) {
    openAFileToWrite("allCodeToExaminate.txt", new ArrayList<String>(Arrays.asList(text)));
  }
  public String getAllCodeToExaminateEDIT() { //PONERLO DONDE TOCA!
    String [] allCodeToExaminate;
    allCodeToExaminate = loadStrings("allCodeToExaminate.txt");
    startText = PApplet.join(allCodeToExaminate, '\n');
    return startText;
  }

  public void readAndSeparateFiles() { //PONERLO DONDE TOCA! TIENE MUCHA RESPONSABILIDAD ... 
    ArrayList <String> mainCode = new <String> ArrayList();
    ArrayList <String> methodsCode = new <String> ArrayList();
    ArrayList <String> attributesCode = new <String> ArrayList();
    ArrayList <String> imports = new <String> ArrayList();
    ArrayList <String> tmp = new <String> ArrayList();
    String [] allCodeToExaminate;
    allCodeToExaminate = loadStrings("allCodeToExaminate.txt");
    if (allCodeToExaminate!=null) {
      for (String content : allCodeToExaminate) {
        if (content.contains("TFGATTRIBUTESTFG")) {
          tmp = attributesCode;
        } else {
          if (content.contains("TFGMAINTFG")) {
            tmp = mainCode;
          } else {
            if (content.contains("TFGMETHODSTFG")) {
              tmp = methodsCode;
            } else {
              if (content.contains("TFGIMPORTSTFG")) {
                tmp = imports;
              } else {
                tmp.add(content);
              }
            }
          }
        }
      }
    }
    /*Escribir en los txt*/
    openAFileToWrite("codigo.txt", mainCode);
    openAFileToWrite("methods.txt", methodsCode);
    openAFileToWrite("globalsVars.txt", attributesCode);
    openAFileToWrite("imports.txt", imports);
    // println("a veeeeeer!!!!!!!",new File(dataPath("")));//LO QUE COSTO!!!
  }

  public void openAFileToWrite(String nameFile, ArrayList linesToSave) {
    try {
      File jf = new File(dataPath("")+"/"+nameFile); //create file in current working directory
      PrintWriter pw = new PrintWriter(jf);
      for (int i = 0; i<linesToSave.size(); i++) {
        pw.println(""+linesToSave.get(i));
      }
      pw.close();
    }
    catch(Exception ex) {
      println("Exception: ", ex.getMessage());
    }
  }


  /*COMPILAAAAR*/
  void crearClassFile() throws Exception {
    /*TRATAMOS DE COMPILAR PARA VER SI HAY PROBLEMAS EN TIEMPO DE COMPILACIÓN*/
    JavaCompiler jc = ToolProvider.getSystemJavaCompiler();
    System.out.println("jc &&&&&&&&&&&&&&&&&6 : "+ jc );
    if(jc != null){
      File jf = createFile(dataPath("")+"/"+"ClassIWantExaminate.java"); // creo un archivo de apoyo en la carpeta data
      StandardJavaFileManager sjfm = jc.getStandardFileManager(null, null, null);
      Iterable fO = sjfm.getJavaFileObjects(jf);
      if (!jc.getTask(null, sjfm, null, null, null, fO).call()) {  
        compilationException = true;
      }
      else{
        jf = createFile(sketchPath("")+"/"+"ClassIWantExaminate.java"); // si se ha podido compilar. Modifico el archivo Java
      }
    }else{
      //decir que no tiene el jdk para poder compilar!!!
      compilationAvaible = false;
    }
  } 
  private File createFile(String file)throws Exception{
    //dataPath("")
     File jf = new File(file); //create file in current working directory
    PrintWriter pw = new PrintWriter(jf);
    pw.println("\nimport java.util.ArrayList;\nimport java.util.*; " + getImports() +"\npublic class ClassIWantExaminate{\nboolean TFGnextTFG;"+
      "\nint TFGidExecuteLineTFG = -1;"+
      "\nHashMap<String,Integer> TFGwhichOneTFG = new HashMap<String,Integer>(); // Para gardar en qué linea estaban situados los métodos! "+
      "\nString TFGwhoIsExecutingNowTFG = \"main\"; // quien se está ejecutando el maín? El método1 o el 2? \n " +
      "public ClassIWantExaminate(){TFGwhichOneTFG.put(\"main\", -1);}"
      +getUserAttributesLines()+
      "\n public void ejecutar(){\n"+getMainMethodLines()+" \n}\n"+getAllLinesUserMethods()+insertMethodToControlExecution()+" \n} \n");
    pw.close();
    return jf;
  }
  boolean compilationAvaible = true;
  boolean getCompilationAvaible(){
    return compilationAvaible;
  }
   String messageNoCompilationAvaible = "No tienes instalado el JDK en las carpetas de processing para comprobar la compilación";
  
  String getMessageNoCompilationAvaible(){
    return this.messageNoCompilationAvaible;
  }
  boolean compilationException = false;
  
  void setCompilationException(boolean compilationException){
    this.compilationException = compilationException;
  }
  boolean getCompilateException(){
    return this.compilationException;
  }
  //getMethodsNeededToControlTheExecution  introduceTokensToControlTheExecution

  String getMainMethodLines() {
    String []  mainMethod = loadStrings("codigo.txt");
    if (mainMethod ==null) {
      return "";
    }
    setTokensControlMain(mainMethod);
    String mainLines = PApplet.join(mainMethod, '\n');
    return mainLines;
  }
  String getUserAttributesLines() { // coge los txt de los atributos
    String []  attributes = loadStrings("globalsVars.txt");
    if (attributes ==null) {
      return "";
    }
    String attributesLines = PApplet.join(attributes, '\n');
    return attributesLines;
  }

  String getAllLinesUserMethods() { 
    /*---*/
    String lines[] = null;
    lines = loadStrings("methods.txt");
    int firstLineOfMethod = -1;
    int lasLineOfMethod = -1;
    String lineWithActualMethod = ""; //
    for (int i = 0; i < lines.length; i++) {
      if (lines[i].trim() != "") { // si viene un espacio en blanco no entres
        if (lines[i].contains("&--")) {
          lasLineOfMethod = i;//
          if (firstLineOfMethod!=-1 && lasLineOfMethod!=firstLineOfMethod) { // saber si se ha cambiado de método
            setTokensControlMethods(lines, firstLineOfMethod, lasLineOfMethod, lineWithActualMethod);
          }
          lines[i] = lines[i].replace("&--", " ");
          lines[i] = lines[i].replace("--&", " ");
          lineWithActualMethod = lines[i];
          firstLineOfMethod = i; //
        }
      }
    }
    setTokensControlMethods(lines, firstLineOfMethod, lines.length, lines[firstLineOfMethod]);
    String methodsLines = PApplet.join(lines, '\n');
    return methodsLines;
  }
  void setTokensControlMain(String [] lines) {
    if (lines != null && lines.length > 0) {
      String tokenWhoIsExecuting = getTokenWhoIsExecuting(lines[0]);
      lines[0]= "esperar(-1); \n esperar(0);\n"+tokenWhoIsExecuting +lines[0]+ "\nTFGwhoIsExecutingNowTFG =\"main\"; ";
      for (int i = 1; i<lines.length; i++) {
        if (isAFinishedCommnad(lines[i])) { // si es una llave no pongas los token si no habrá error!!!
          tokenWhoIsExecuting = getTokenWhoIsExecuting(lines[i]);
          lines[i]= "esperar("+i+"); \n " +tokenWhoIsExecuting+lines[i]+ "\nTFGwhoIsExecutingNowTFG =\"main\"; ";
        }
      }
      int i = lines.length;
      lines[i-1] = lines[i-1] + "esperar("+ i +");" ;
    }
  }
  boolean isAFinishedCommnad(String line) {
    if (line.trim().equals("{") || line.trim().equals("}")) {
      return false;
    }
    if (line.contains(";") && !line.trim().startsWith("/*")) {
      return true;
    }
    return false;
  }
  String getTokenWhoIsExecuting(String line) {
    String token = "";
    for (String keyText : linesStores.keySet()) {
      if (line.contains(keyText)) {
        return "TFGwhoIsExecutingNowTFG = \""+keyText+"\";\n";
      }
    } 
    return token;
  }
  void setTokensControlMethods(String[] lines, int start, int last, String methodExecuted) {
    int num = 0;
    String tokenWhoIsExecuting = "";
    for (int i = start+1; i<last-1; i++) {
       if (isAFinishedCommnad(lines[i])) {
           tokenWhoIsExecuting = getTokenWhoIsExecuting(lines[i]);
           if(tokenWhoIsExecuting == ""){// si no hay ni un token, es decir no llaman a ni un método //
           //vuelve a poner que están ejecutando el método actual //
             tokenWhoIsExecuting = getTokenWhoIsExecuting(methodExecuted);
             lines[i] = tokenWhoIsExecuting + "esperar("+num+");"+"\n"+ lines[i];
           } 
           else{
              lines[i] = "esperar("+num+");\n"+tokenWhoIsExecuting + lines[i];
           }
       }
      num++;
    }
  }
  private String insertMethodToControlExecution() {
    String controlLines = " \nvoid esperar(int i ) {\n try {\n   TFGwhichOneTFG.put(TFGwhoIsExecutingNowTFG, i);\n   sleep();\n}\n catch(Exception ex) {"+
      "  System.out.println(\"Exception cuando quería dormir :( \");\n}\n}"+"\nvoid sleep() throws Exception {\n while (TFGnextTFG==false) {\n"+ 
      "   Thread.sleep (100);\n  }\n   TFGnextTFG = !TFGnextTFG;\n}"+
      "\nvoid wakeUp() {\n  TFGnextTFG = !TFGnextTFG;\n}" +
      "\nint getIdExecuteLine(){ // para saber el número de línea exacta de ejecución. És decir, por donde va el programa"+
      "\n  return TFGwhichOneTFG.get(TFGwhoIsExecutingNowTFG);\n}"+
      "\nString getWhoIsExecutingNow(){ // para saber si se está ejecutando el main o lo métodos para posteriormente mostrarlo"+
      "\n  return this.TFGwhoIsExecutingNowTFG;\n}";
    return controlLines;
  }
}