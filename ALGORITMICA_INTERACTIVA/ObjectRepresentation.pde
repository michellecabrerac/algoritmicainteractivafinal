class ObjectRepresentation{
  int posX;
  int posY;
  String id;
  int numPosition;
  String tipus;
  public ObjectRepresentation(int posX,int posY, String id,int numPosition, String tipus){
    setPositions(posX,posY);
    setId(id);
    setNumPosition(numPosition);
    setTipus(tipus);
  }
  void setTipus (String tipus){
    this.tipus = tipus;
  }
  String getTipus(){
    return this.tipus;
  }
  void setNumPosition(int numPosition){
    this.numPosition = numPosition;
  }
  void setPositions(int posX, int posY){
    this.posX = posX;
    this.posY = posY;
  }
  void setXPositions(int posX){
    this.posX = posX;
  }
    void setYPositions(int posY){
    this.posY = posY;
  }
  void setId(String id){
    this.id = id;
  }
   int getPositionX(){
     return this.posX;
   }
  
   int getPositionY(){
     return this.posY;
  }
  
  String getId(){
    return this.id;
  }
  int getNumPosition(){
    return this.numPosition;
  }
  void drawObject(){
    //background(255);
    switch(tipus){
      case "queue":
      fill(255,125,6);
        ellipse(posX, posY, 55, 55);
      break;
      case "stack":
      break;
      case "list":
      break;
    }
  }
}