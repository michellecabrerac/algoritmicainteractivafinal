class PresentaArray{ // Esta clase sera la encargada de crearse si es necesario

Map almacen;
Object[] array;
float x,y,x2,y2, newX;
 
PresentaArray getArray(String identificador, Object[] obj, float x, float y, Map almacen){
  almacen.put(identificador, new PresentaArray(obj,x,y, almacen)); //<>// //<>//
  return (PresentaArray)almacen.get(identificador); //<>// //<>//
}

public PresentaArray(Object[] obj,float x, float y, Map almacen){
  if(obj == null){  this.array = new Object[0];}
  else{  this.array = obj; }
  this.almacen = almacen; //<>// //<>//
  this.x = x+100; //<>// //<>//
  this.y = y-20; //<>// //<>//
}

void setArray(Object[] obj){
  this.array = obj;
}

void setCoord(float x, Float y){
 this.x = x;
 this.y = y;
}

Object[] getObjectArray(){
 return array;
} 

Float [] getCoordenadas(){
 Float[] coord = new Float[2];
 coord[0] = x; //<>// //<>// //<>//
 coord[1] = x; //<>// //<>// //<>//
 return coord; //<>// //<>// //<>//
}

void imprimir(boolean isPrimitive){
  rect(x, y, 55*array.length, 40);
  for(int i = 0; i < array.length; i++){
    line(x+55*(i+1), y, x+55*(i+1), y+40);
    fill(40,40,40);
    textSize(12); //<>// //<>//
    Object o = array[i]; //<>// //<>// //<>//
    if(o == null){ newX = getCooredenadasXYCentrar("null", x);  } //<>// //<>//
    else{  newX = getCooredenadasXYCentrar(o, x);}
    x2 = newX+55*i;
    y2 = y + 30;//y+55+15; // y+35
    fill(255);
    text("["+i+"]", x+25+55*i, y2+20);
    fill(40,40,40);
    if(newX == x){ //<>// //<>//
      if(array[i] != null){  text(array[i].toString().substring(0,5)+"...",  x2+5, y2 ); } //<>// //<>//
      else{  
        if(isPrimitive){ text("0",x2+5, y2 ); }
        else{text("null",x2+5, y2 ); }
      } 
    } //<>// //<>//
    else{
      if(array[i] != null){  text(array[i].toString(),  x2, y2 );  } //<>// //<>//
      else{  
        if(isPrimitive){
          text("0",x2, y2 );  
        }
        else{text("null",x2, y2 );  }
      }
    }
    fill(255);
  } 
}
 
float getCooredenadasXYCentrar(Object o, float x){
  int tam = o.toString().length();  
  if (tam <= 2 ){  return x+25;  }
  if(tam >= 7){  return x;  }
  return x+5;
}

}