interface ModelDataStructures {
   String performAction(String action);
   int getSize();
   public String getClassType();
   String [] getObjectsIDs();
}