    /*para compilar*/
import javax.tools.JavaCompiler; 
import javax.tools.ToolProvider;
import javax.tools.StandardJavaFileManager;
/*fi compilar*/
import g4p_controls.*; // FOR TEXT AREA
int bgcol = 32;
String startText;
/*FOR THE TEXTAREA - BUTTONS*/
GButton nextButton, compilebutton, editButton, saveButton;
GButton queueButton, stackButton, listButton;
GTextArea textArea;
//FI TEXT AREA
ControllerCodeSpy controllerCodeSpy;
ControllerTutorial controllerTutorial;
ControllerDataStructures controllerStructures;
void setup() {
  /*INITIAL SETTING*/
  textFont(createFont("SourceCodePro-Regular.ttf", 36));
  getInitialSetting();
  /*FOR THE TUTORIAL*/
  ViewTutorial viewTutorial = new ViewTutorial();
  controllerTutorial = new ControllerTutorial(viewTutorial);
  /*FOR THE STRUCTURES ANIMATION*/
  controllerStructures = new ControllerDataStructures();
  /*ModelDataStructures modelStructures = new ModelDataStructures();
  ViewDataStructures viewStructures = new ViewDataStructures();*/
  //controllerStructures.setModelAndView(modelStructures, viewStructures);
  queueButton = new GButton(this, 100, 200, 100, 40, "Queue");
  stackButton = new GButton(this, 100, 300, 100, 40, "Stack");
  listButton = new GButton(this, 100, 400, 100, 40, "List");
  queueButton.setVisible(false);
  stackButton.setVisible(false);
  listButton.setVisible(false);
  /*FOR SPY THE CODE*/

  ModelCodeSpy modelCodeSpy = new ModelCodeSpy(); /**/

  ViewCodeSpy viewCodeSpy = new ViewCodeSpy();
  controllerCodeSpy = new ControllerCodeSpy(modelCodeSpy, viewCodeSpy);
  controllerCodeSpy.setModelAndView(modelCodeSpy, viewCodeSpy);
  thread("executeMainMethodOfExaminatedClass"); // iNICIAMOS  UN THREAD Y LA EJECUCIÓN
  /**/
  background(255); //
  textSize(lettersSize+10);
    /*FOR THE BUTTONS OF EDITOR*/
   nextButton= new GButton(this, 400, height -100, 100, 40, "Next");
   nextButton.setVisible(false);
   compilebutton = new GButton(this, 100, height -100, 100, 40, "Compile");
   compilebutton.setVisible(false);
   editButton = new GButton(this, 300, height -100, 100, 40, "Edit");
   editButton.setVisible(false);
   saveButton = new GButton(this, 200, height -100, 100, 40, "Save");
   saveButton.setVisible(false);
  // StyledString ss;  new StyledString("Subscript"); 
   textArea = new GTextArea(this, 80, 20, width/2, height-150, G4P.SCROLLBARS_BOTH | G4P.SCROLLBARS_AUTOHIDE);
   //textArea.moveCaretTo(0,0);
   textArea.setVisible(false);
   textArea.setTextEditEnabled(false);
}

/***  ALL ATTRIBUTES  ***/
/*SETTING ATTRIBUTES*/
int widthScreen = 0;
int heightScreen = 0;
color backgroundColor = -999999999;
color lettersColor = -999999999;
int lettersSize = -1;
int letterSizeEditor = 18;
/*ATTRIBUTES MENU*/
final int MENU = 1;
final int SPY = 2;
final int STRUCTURES = 3;
final int TUTORIAL = 4;
final int ERROR = 5;
boolean thereAreAnError = false;
int showMeOnScreen = MENU;
/***  ALL METHODS  ***/
void draw() {
 //cursor(ARROW);
 /*if(mouseIsOver()){
   cursor(HAND);
  }*/

  try{
      switch(showMeOnScreen) {
  case MENU: 
    showMenu(); 
    break;
  case SPY: 
    showCodeSpy(); 
    break;
    case STRUCTURES: 
    //background(255);
    showDataStructures(); 
    break;
  case TUTORIAL:
    showTutorial();
    break;
    //case ERROR: showError(msg, whichFile); break;
  }
  }catch(Exception ex){println("Exception: ",ex);}

  if (showMeOnScreen != MENU) {
    printGoBackTotheMenu();
  }
// checkCursor();
}
void printGoBackTotheMenu() {
  textSize(lettersSize-5);
  fill(255, 0, 0);
  text("<- Go to Menu", width-200, height-100);
}
void  mousePressed() {
  checkPushButtons();
  checkPushButtonNext();
  checkPushLink();
  checkPushGoToMenu();
  checkPushActionDataStructures();
}
void checkPushActionDataStructures(){
  if (showMeOnScreen == STRUCTURES) {
    controllerStructures.buttonPressed(mouseX, mouseY);
  }
}
/*COORDINATES BUTTONS MENÚ*/
int codeSpyButtonX = 350;
int codeSpyButtonY = 120;
int structuresButtonX = 400;
int structuresButtonY = 210;
int tutorialButtonX = 450;
int tutorialButtonY = 300;
int widthButtons = 450;
int heightButtons = 35;

void showMenu() {
  noFill();
  fill(0, 2, 70, 10);
  rect(codeSpyButtonX, codeSpyButtonY, widthButtons, heightButtons, 7);
  rect(structuresButtonX, structuresButtonY, widthButtons, heightButtons, 7);
  rect(tutorialButtonX, tutorialButtonY, widthButtons, heightButtons, 7);
  fill(255);
  int a = 150;
  text("Code spy", codeSpyButtonX+a, codeSpyButtonY+30); 
  text("Estructuras", structuresButtonX+a, structuresButtonY+30); 
  text("Tutorial", tutorialButtonX+a, tutorialButtonY+30);
}
boolean showTextAvaibleOption = false;
void checkPushButtons() {
  if (showMeOnScreen == MENU) {
    if (mouseX>codeSpyButtonX && mouseX< codeSpyButtonX+widthButtons && mouseY>codeSpyButtonY && mouseY<codeSpyButtonY+heightButtons) {
      changeScreenCodeSpy();
    } else {
      if (mouseX>structuresButtonX && mouseX< structuresButtonX+widthButtons && mouseY>structuresButtonY && mouseY<structuresButtonY+heightButtons) {
         showMeOnScreen = STRUCTURES;
         showTextAvaibleOption = true;
         showStucturesButton(); // muestra los botones correspondiente a las estructuras disponibles
      } else {
        if (mouseX>tutorialButtonX && mouseX< tutorialButtonX+widthButtons && mouseY>tutorialButtonY && mouseY<tutorialButtonY+heightButtons) {
          showMeOnScreen = TUTORIAL;
        }
      }
    }
  }
}

/*Para poner cmabiar el cursos a manita*/
boolean mouseIsOver() {
  if (showMeOnScreen == MENU) {
    if (mouseX>codeSpyButtonX && mouseX< codeSpyButtonX+widthButtons && mouseY>codeSpyButtonY && mouseY<codeSpyButtonY+heightButtons) {
      cursor(HAND);
      return true;
    } else {
      if (mouseX>structuresButtonX && mouseX< structuresButtonX+widthButtons && mouseY>structuresButtonY && mouseY<structuresButtonY+heightButtons) {
        cursor(HAND);
        return true;
      } else {
        if (mouseX>tutorialButtonX && mouseX< tutorialButtonX+widthButtons && mouseY>tutorialButtonY && mouseY<tutorialButtonY+heightButtons) {
          cursor(HAND);       
          return true;
        }
        return false;
      }
    }
  }
  return false;
}



void checkPushButtonNext() {
  if (showMeOnScreen == SPY) {
    controllerCodeSpy.checkPushButtonNext(mouseX, mouseY);
  }
}

void checkPushLink() {
  if (showMeOnScreen == TUTORIAL) {
    controllerTutorial.checkPushLink(mouseX, mouseY);
  }
}

void checkPushGoToMenu() {
  if (mouseX>width-200 && mouseX<width-80 && mouseY>height-120 && mouseY<height-90) {
    initAll();
    showMeOnScreen = MENU;
  }
}

void changeScreenCodeSpy() {
  if (!thereAreAnError) {
    showMeOnScreen = SPY;
    //nextButton= new GButton(this, 300, height -100, 100, 40, "Next");
    //compilebutton = new GButton(this, 100, height -100, 100, 40, "Compile");
   // saveButton.setVisible(true); // 
    editButton.setVisible(true); // 
    controllerCodeSpy.showTextArea(textArea,nextButton,compilebutton); // X,y, ancho, alto
  } else {
    showMeOnScreen = ERROR;
  }
}
void debug() {
}
void showCodeSpy() {
  controllerCodeSpy.printAll();
}
void checkCursor(){
  if(mouseIsOver()){
     cursor(HAND);
  }
}
void showDataStructures() {
    background(255);
  if(showTextAvaibleOption == true){
    showAvaibleOption();
  }
  controllerStructures.printStructuresAnimation();
}
void showAvaibleOption(){
  textSize(lettersSize-5);
   fill(0);
  text("Selecciona una estructura para ver su animación:", 100, 100);
}
void showTutorial() {
  controllerTutorial.printInfoTutorial();
}
void executeMainMethodOfExaminatedClass() {
  controllerCodeSpy.executeMainMethodOfExaminatedClass();
}
void initAll() {
  /*FOR THE TUTORIAL*/
  ViewTutorial viewTutorial = new ViewTutorial();
  controllerTutorial = new ControllerTutorial(viewTutorial);
  
  /*FOR THE STRUCTURES ANIMATION*/
  controllerStructures = new ControllerDataStructures();
  showTextAvaibleOption = false;
  /*ModelDataStructures modelStructures = new ModelDataStructures();
  ViewDataStructures viewStructures = new ViewDataStructures();
  controllerStructures.setModelAndView(modelStructures, viewStructures);*/

  /*FOR SPY THE CODE*/
  initCodeSpy();
  /*FOR textArea*/
  textArea.setVisible(false);
  nextButton.setVisible(false);
  compilebutton.setVisible(false);
  saveButton.setVisible(false);
  editButton.setVisible(false); // 
  
  // FOR STRUCTURES
   queueButton.setVisible(false);
  stackButton.setVisible(false);
  listButton.setVisible(false);
}
void initCodeSpy(){
  ModelCodeSpy modelCodeSpy = new ModelCodeSpy(); /**/
  ViewCodeSpy viewCodeSpy = new ViewCodeSpy();
  controllerCodeSpy = new ControllerCodeSpy(modelCodeSpy, viewCodeSpy);
  controllerCodeSpy.setModelAndView(modelCodeSpy, viewCodeSpy);
  thread("executeMainMethodOfExaminatedClass"); // iNICIAMOS  UN THREAD Y LA EJECUCIÓN
  background(255); 
  textSize(lettersSize+10);
  textArea.clearStyles(); // del textArea quitale los estilos que se hayan quedado
}
/*METHODS TO GET THE SETTINGS*/
void getInitialSetting() {
  getAllSettings();
  setSizeScreen();
  setColorBackground();
  setLettersColor();
  setLettersSize();
}
void setSizeScreen() {
  if (widthScreen == 0) {
    widthScreen = 1500;
  } 
  if (heightScreen == 0) {
    heightScreen = 1000;
  } 
  surface.setSize(widthScreen, heightScreen); // To establish the size of the sketch
}

void setColorBackground() {
  if (backgroundColor == -999999999) {
    backgroundColor = color(39, 40, 34);
  }
}
void setLettersColor() {
  if (lettersColor == -999999999) {
    lettersColor = color(255);
  }
}
void setLettersSize() {
  if (lettersSize == -1) {
    lettersSize = 25;
  }
}
void getAllSettings() {    
  String lines[] = loadStrings("settings/EnvironmentSettings.txt");
  String[] splitLinea;
  int result, tmp;
  for (int i = 0; i<lines.length; i++) {
    result = lines[i].indexOf("=");
    //esComentario = lines[i].indexOf("#");
    if (result != -1) {
      splitLinea = lines[i].split("=");
      if (splitLinea.length >= 2) {
        switch(splitLinea[0].replace(" ", "")) {
        case "widthScreen":
          tmp = parseInt(splitLinea[1].trim());
          if (tmp>0) { 
            widthScreen = tmp;
          }
          break;
        case "heightScreen":
          tmp = parseInt(splitLinea[1].trim());
          if (tmp>0) {  
            heightScreen =  tmp;
          }
          break;
        case "backgroundColor":
            String c = splitLinea[1].trim();
            c = "FF" + c.substring(1);
            backgroundColor = unhex(c);
          break;
        case "lettersColor":
            String col = splitLinea[1].trim();
            col = "FF" + col.substring(1);
            lettersColor = unhex(col);
          break;
        case "lettersSize":
          tmp = parseInt(splitLinea[1].trim());
          if (tmp>0) {  
            lettersSize = tmp;
          }
          break;
          case "letterSizeEditor":
            tmp = parseInt(splitLinea[1].trim());
            if (tmp>0) {  
              letterSizeEditor = tmp;
            }
          break;
        }
      }
    }
  }
}
/*FOR THE EDITOR*/

public void handleButtonEvents(GButton button, GEvent event) { 
  if (event == GEvent.CLICKED) {
    if (button == nextButton){
      controllerCodeSpy.pushNextButton();
      nextButton.setVisible(true);
      compilebutton.setVisible(false);
      editButton.setVisible(true);
      saveButton.setVisible(false);
    }
    if(button == compilebutton){
      controllerCodeSpy.pushCompileButton();
      compilebutton.setVisible(false);
      editButton.setVisible(true);
      saveButton.setVisible(false);
      if(controllerCodeSpy.thereAreCompilationException()){
        nextButton.setVisible(false);
      }
      else{
         nextButton.setVisible(true);
      }

    }
   if(button == editButton){
      controllerCodeSpy.pushEditButton();
      editButton.setVisible(false);
      nextButton.setVisible(false);
      compilebutton.setVisible(false);
      saveButton.setVisible(true);
    }
    if(button == saveButton){
      controllerCodeSpy.pushSaveButton();
      editButton.setVisible(true);
      compilebutton.setVisible(true);
      nextButton.setVisible(false);
      saveButton.setVisible(false);
      //nextButton.setLocalColorScheme(index); // el por default es el 6
      //nextButton.setEnabled(false);
    }
    if(button == queueButton){
      showTextAvaibleOption = false;
      background(255);
      hideStucturesButton();
      controllerStructures.createStructure("QUEUE");
    } 
    if(button == stackButton){
      showTextAvaibleOption = false;
      background(255);
      hideStucturesButton();
      controllerStructures.createStructure("STACK");
    } 
   if(button == listButton){
     showTextAvaibleOption = false;
     background(255);
      hideStucturesButton();
      controllerStructures.createStructure("LIST");
    } 
  }
}
void hideStucturesButton(){
  queueButton.setVisible(false);
  stackButton.setVisible(false);
  listButton.setVisible(false);
}
void showStucturesButton(){
  queueButton.setVisible(true);
  stackButton.setVisible(true);
  listButton.setVisible(true);
}