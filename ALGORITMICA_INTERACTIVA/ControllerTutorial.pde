class ControllerTutorial {
  String [] lines;
  ViewTutorial viewTutorial;
  public ControllerTutorial(ViewTutorial viewTutorial) {
    this.viewTutorial = viewTutorial;
    lines = new String[0];
    readExternalFile();
    viewTutorial.setInfoTutorial(lines);
  }
  String[] getLines() {
    return lines;
  }
  void printInfoTutorial(){
    viewTutorial.printInfoTutorial();
  }
  public void readExternalFile() {
    lines = loadStrings("tutorial.txt");
    if (lines == null) { // f the file is not available or an error occurs, null will be returned
      lines =  new String[0];
    }
  }
  void checkPushLink(int x, int y){
    viewTutorial.checkPushLink(x,y);
  }
}