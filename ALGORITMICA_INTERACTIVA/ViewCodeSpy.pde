import java.awt.Font; // para la fuente del editor
import java.awt.Color; // para poder subrayar en el text area
class ViewCodeSpy {
  ArrayList<String> linesToPrint = new ArrayList<String>();

  int whichLineIsExecuting = -1;
  //Las posiciones de las lineas que pintara. La 1º linea esta en la posición X1,y1, etc
  int [][] positions= new int [10][10]; // inicialización por default
  //int textSize = 25;
  int colorReservedWord = -65408;

  //PImage img= loadImage("azul.png");
  void setLinePositions(int [][] positions) {
    this.positions = positions;
  }
  void setLineToPrint(ArrayList lines, int beginOfLine) {
    linesToPrint = lines;
    this.beginOfLine = beginOfLine;
  }
 void setLineToPrintArray(String [] lines, int beginOfLine) {
    linesToPrintArray = lines;
    this.beginOfLine = beginOfLine;
  }
  ArrayList getLinesToPrint() {
    if (linesToPrint == null) {
      return new ArrayList();
    }
    return this.linesToPrint;
  }
  int positionLastLine;
/*  void printCodePresentation() { Estaría debajo del textArea
    background(39, 40, 34);
    fill(colorWord);
    int y = 0;
    int indexLine = 0 ;
    for (String line : linesToPrint) {
      if(positions!=null && indexLine<positions.length && positions[indexLine] != null){
         textSize(lettersSize);
         println("lettersSize: ", lettersSize);
        //float x = 100;
        float x = positions[indexLine][0];
        y = positions[indexLine][1];
        try {
          if (!line.trim().equals("")&&!controllerUtils.isAComment(line)) {
            for (String word : line.split(" ")) {
              float sw = textWidth(word); // Te da la medida del texto
              fill(255);
              x = x + 10; //espacio entre word
              if (controllerUtils.isAReservedWord(word)) { 
                fill(colorReservedWord);
              }
              text(word, x, y); 
              x = x+sw;
            }
          }
        }
        catch(Exception ex) {
          println("OJOO EXCEPTION EN PresenctacionCodigo -> imprimir " +ex);
        }   
        y = positions[indexLine][1];
        indexLine++;
        }
    }  // fi if
    illuminateCodeLine(); // una vez dibujado las lineas de código ilumina a quien toque
    positionLastLine = y+20;
    image(img, 100, y+20, sizeNextButton, sizeNextButton);
  }  */
  int sizeNextButton = 70;
  boolean checkPushButtonNext(int x, int y) { // le dice al controlador true si es que han pulsado el botón
    if (y>positionLastLine && y<positionLastLine+sizeNextButton && x>100 &&x<100+sizeNextButton) {
      return true;
    }
    return false;
  }
  void setWhichLineIsExecuting(int whichLineIsExecuting) {
    this.whichLineIsExecuting = whichLineIsExecuting;
  }
  int beginOfLine = 0; // si es un método la primera línes referente a la cabecera no cuenta!!!
 /* void illuminateCodeLine() { //Estaría debajo del textArea
    if (whichLineIsExecuting != -1) {
      noFill();
      fill(0, 2, 70, 10);
      //ilumina la linea que se va a ejecutar
      if( whichLineIsExecuting+beginOfLine<positions.length){ // con +beginOfLine evitamos el Arraybound
        rect(this.positions[whichLineIsExecuting+beginOfLine][0], this.positions[whichLineIsExecuting+beginOfLine][1]-25, 450, 35, 7);
      }else{
        println("ViewCoseSpy- PROBLEMA CON SINCRONIZACIÓN - illuminateCodeLine");
      }
      fill(255);
    }
  } */

  String[] valueslinesToPrint = {"CodeSpy"};
  HashMap <String ,Object []> arrayValuesToPrint;
  HashMap <String ,Boolean> isPrimitiveArray;
  String [] linesToPrintArray = {"Enter your code"};

  void setValueslinesToPrint(String[] valueslinesToPrint) {
    this.valueslinesToPrint = valueslinesToPrint;
  }
 void setArrayValueslinesToPrint(HashMap <String ,Object []> arrayValuesToPrint) {
    this.arrayValuesToPrint = arrayValuesToPrint;
  }
   void setIsPrimitiveArray(HashMap <String ,Boolean> isPrimitiveArray) {
    this.isPrimitiveArray = isPrimitiveArray;
  }
  int positionValuesLinesX = 800;
  int positionY; // posisción y de los valores representados como String
  /*VALUES PRESENTATION*/
  void printValuesPresentation() {
    background(backgroundColor); //  Esto es necesario porque si no no refresca 
    textSize(lettersSize);
    fill(lettersColor);
    if(positions != null && positions.length != 0){
       positionY=positions[0][1]; // que comience desde la primera y de positions
      for (String value : valueslinesToPrint) {
        if(value != null){ // validar que lo que te pasan por parámetro no sea null. 
        //Ya que viene null porque hay attributos de la clase a examinar reservados al TFG
        text(value, positionValuesLinesX, positionY);
        positionY = positionY + 50;
        }
      }
    }
  }
  Map almacen = new HashMap();
  PresentaArray pArray = new PresentaArray(null, 0,0,almacen);
  void printArrays(){ 
    textSize(lettersSize);
    float y = positionY;
    float x = positionValuesLinesX;
    if(arrayValuesToPrint != null){
        for(String keys: arrayValuesToPrint.keySet()){
           textSize(lettersSize);
          text(keys+":", x, y);
          if(arrayValuesToPrint.get(keys) == null){
            text("null",x+150,y);
          }
          else{
            pArray.getArray( keys+":", arrayValuesToPrint.get(keys), x+20 , y, almacen).imprimir(isPrimitiveArray.get(keys)); 
          }
          y = y+50;
      }
    }
  }
float getCooredenadasXYCentrar(Object o, float x){
  int tam = o.toString().length();  
  if (tam <= 2 ){  return x+25;  }
  if(tam >= 7){  return x;  }
  return x+5;
}

/**/
  void setMessageException(String exceptionMessage, int x, int y){
    // x = width/2+150  y=50
    textSize(lettersSize-10);
    fill(0,255,0);
    float sw = textWidth(exceptionMessage); // para ver cuanto ocupa el texto
    if(sw > width/2 - 150){ // si el mensaje más largo que la mitad de la pantalla... PARTE EL STRING
    text(exceptionMessage.substring(0,exceptionMessage.length()/2),x, y);
    text(exceptionMessage.substring(exceptionMessage.length()/2, exceptionMessage.length()), x, y+15);
    
    }else{
          text(exceptionMessage, x, y);
    }
  }
  GButton btnNext, btnCompile;
  GTextArea textArea;
  void showTextArea(GTextArea textArea,GButton buttonNext, GButton buttonCompile ){ // Solo se debe invocar 1 vez
    this.textArea = textArea;
    textArea.setVisible(true);
    textArea.setFont(new Font("", Font.PLAIN, letterSizeEditor)); //ponerle tamaño a las letras
   String[] paragraphs = linesToPrintArray;
    startText = PApplet.join(paragraphs, '\n');
    if(startText != null){
     textArea.setText(startText, (width/2)-40);
    }
    // Set some default text
    textArea.setPromptText("Please enter your code");
    /*CONFIGURACIÓN BOTONES*/
   // buttonNext.setVisible(true);
    //buttonCompile.setVisible(true);
    btnNext = buttonNext;
    btnCompile = buttonCompile;
  }
  void setLinesInTextArea(){
    textArea.clearStyles(); // limpia el LighHigth
    String[] paragraphs = linesToPrintArray;
    startText = PApplet.join(paragraphs, '\n');
    if(startText != null){
      this.textArea.setText(startText, (width/2)-40);
    } //<>//
 //<>// //<>//
    if(whichLineIsExecuting != -1){
      //addStyle (TextAttribute attr, Object value, int lineNo, int charStart, int charEnd)
      int charEnd = this.textArea.getText(this.whichLineIsExecuting+beginOfLine).length();
      this.textArea.addStyle(G4P.BACKGROUND, new Color(250, 210, 001), this.whichLineIsExecuting+beginOfLine, 0, charEnd); // de background amarillo
      this.textArea.addStyle(G4P.FOREGROUND, new Color(0), this.whichLineIsExecuting+beginOfLine, 0, charEnd); // que las letras sean negras
    }
  }
  String [] getTextTextarea(){
     return this.textArea.getTextAsArray();
  }
  void editTextarea(String text){
   this.textArea.setText(text, (width / 2) -40 );
  }

  void pushEditButton(){
    textArea.setTextEditEnabled(true);
  }
  void pushCompileButton(){
   textArea.setTextEditEnabled(false);
  }
  void pushSaveButton(){
    textArea.setTextEditEnabled(false);
  }
  void pushNextButton(){ 
    textArea.setTextEditEnabled(false);
  }
}