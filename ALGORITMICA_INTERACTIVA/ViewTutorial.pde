class ViewTutorial {
  String [] infoTutorial = {"Tutorial"};
  //int textSize = 20;
  void setInfoTutorial( String [] infoTutorial) {
    this.infoTutorial = infoTutorial;
  }
  void printInfoTutorial() {
    textSize(lettersSize-5);
    background(255);
    int x = 100;
    int y = 100;
    fill(0);
    for (int i = 0; i < infoTutorial.length; i++ ) {
      text(infoTutorial[i], x, y);
      y = y + 30;
    }
    fill(0,0,255);
    text("link Documentación", x, y+20);
    linkY = y+20;
  }
  int linkY;
  int linkX = 100;
  void checkPushLink(int x, int y) {
    if (y > linkY-20 && y < linkY+50 && x>linkX-10 && x<linkX+230 ) {
      link("https://www.dropbox.com/sh/ib5n4shkf1z7g9c/AADUqJl1jiTwS2LeXBieMhKXa?dl=0");
    }
  }
}