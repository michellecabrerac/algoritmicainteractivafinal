class ModelListStructures implements ModelDataStructures {
    List list;
  String typeOfList;
  public ModelListStructures(String type){
    setTypeOfList(type);
  }
  private void setTypeOfList(String type){
    String classType = type.toUpperCase();
      switch(classType){
      case "ARRAYLIST":
        typeOfList = "ArrayList";
        list =  new ArrayList(5);
      break;
      case "LINKEDLIST":
         typeOfList = "LinkedList";
         list =  new LinkedList();
      break;
      default: 
      typeOfList = "ArrayList";
      list =  new ArrayList(5);
    }
  }
  public String getClassType(){
    return this.typeOfList;
  }

  String performAction(String action){
    String result = "";
      switch(action){
      case "add":
      result = result + list.add(new Object());
      break;
      case "clear":
       list.clear();
      break;
      case "isEmpty":
       result = result + list.isEmpty();
      break;
      case "iterator":
        result = result + list.iterator();
      break;
      case "listIterator":
       result = result + list.listIterator();
      break;
      case "remove":
       result = result + list.remove(0);
      break;
      case "toArray":
       result = result +  list.toArray();
      break;
    }
    return result;
  }
  int getSize(){
    return list.size();
  }
 String[] getObjectsIDs(){
    Object [] array = list.toArray();
    String [] ids = new String [array.length];
    for(int i = 0; i<array.length; i++){
      ids[i] = array[i].toString();
    }
    return ids;
  }
}