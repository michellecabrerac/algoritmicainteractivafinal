class ViewStructuresQueue implements ViewDataStructures{
 // ControllerView controllerView;
   /*PAra dibujar las animaciones*/
   String [] idObjects = new String[0];
   List<ObjectRepresentation> objectsList = new ArrayList();
   String [] lastIdObjects = new String[0];
  /* void checkObject(){

 //<>//
     if(idObjects.length != objectsList.size()){
       if(idObjects.length > objectsList.size()){ // add //<>//
         if(idObjects != null && idObjects.length>0){ //<>//
           objectsList.add(new ObjectRepresentation(150+ 55*(idObjects.length-1) ,100, idObjects[idObjects.length-1],objectsList.size(), "queue"));
         }  
         lastIdObjects = idObjects;  //<>//
       }
       else{ // remove
         if(idObjects != null){
           createAnimationOfObjects();
           lastIdObjects = idObjects; 
         }

       }
     }
   }
   void createAnimationOfObjects(){ //<>//
     if(idObjects.length < lastIdObjects.length){ // significa que han sacado un objeto //<>//
         if(idObjects.length > 0 && lastIdObjects.length > 0  &&  idObjects[0] == lastIdObjects[0]){ //<>//
           desplaceObjects("right"); //<>//
        } //<>//
        else{ //<>//
              desplaceObjects("left");
        }
     }
 //<>//
   }
   void desplaceObjects(String tipus){
     
     switch(tipus){
       case "left":
       if(objectsList.size() > 1){
          int positionPivot = objectsList.get(0).getPositionX(); //<>//
           objectsList.remove(0); //<>//
           drawObjects(); // después d eeliminar pinta
            delay(1000); //<>//
            for(int k=0; k<objectsList.size(); k++){ //<>//
               delay(100);
              int positionIAm = objectsList.get(k).getPositionX(); //<>//
              //int nextPosition = objectsList.get(k+1).getPositionX();
              int positionIWantGo = positionPivot;
              for(int i=positionIAm; i>0; i--){
                 delay(10); //<>//
                 background(255);
                objectsList.get(k).setXPositions(i--);
                objectsList.get(k).drawObject();
              }
               positionPivot = positionIAm;
            }
       }
       else{
         if(objectsList.size() != 0){
           int i = 0;
           while(i<1000){
             background(255,255,0);
             objectsList.get(0).setXPositions(i);
             objectsList.get(0).drawObject();
             i++;
           }
            objectsList.remove(0);
         }

       }
       break;
     }

   }
   void drawObjects(){
     //background(255);
     for(int i = 0; i<objectsList.size(); i++){
         objectsList.get(i).drawObject();
     }
   } */
   void setIdsObjects(String [] idObjects){
     this.idObjects = idObjects;
   }
  /**/
  String actionReturn;
  int size;
  String classType;
  void setSize(int size){
    this.size = size;
  }
  int getSize(){
  return this.size;
  }
 void setClassType(String classType){
   this.classType = classType;
 }
  public ViewStructuresQueue(){
    // this.controllerView = new ControllerView();
     this.size = 0;
     this.actionReturn = "";
     this.classType = "";
  }
 /* void setViewController(ControllerView controllerView){
    this.controllerView = controllerView;
  }*/
  void drawStructure(){
   drawObjects();
   //drawSpace();

    //checkObject(); //comprueba si ha habido cambios... y haz la animación
   // drawObjects(); //dibuja

   // createAnimationOfObjects();
    drawActionButtons();
      drawActionReturn();
    drawClassType();
  }
  void drawClassType(){
    fill(0, 255, 153);
    text(classType,width-300,30);
  }
   void drawObjects(){
    if(idObjects != null){
      fill(0, 102, 153);
      for(int i = 0; i<idObjects.length; i++){ // ARREGLAR!!! MIRAR SI SE ESTA HACIENDO BIEN!!
        text(""+(i+1), 150+ 55*i, 50 );
        ellipse(150+ 55*i, 100, 55, 55);
        text(idObjects[i],150+ 55*i,100+50+(20*i)); // para que el id aparezca al lado del la representación del objeto
      }   
    } //<>//
  }
  void drawSpace(){

  }
  void drawActionReturn(){
    int coordinatesReturnResultX = width-350;
    int coordinatesReturnResultY = height - 500;
    noFill();
    rect(coordinatesReturnResultX, coordinatesReturnResultY, 350,280, 7);
    text("Result: ",coordinatesReturnResultX,coordinatesReturnResultY);
    fill(255,0,0);
    text(actionReturn,coordinatesReturnResultX+30,coordinatesReturnResultY+100);
  }
  int coodinatesAddButtonX = 50;
  int coodinatesAddButtonY = 300;
  int coodinatesRemoveButtonX = 50;
  int coodinatesRemoveButtonY = 350;
  int coodinatesElementButtonX = 50;
  int coodinatesElementButtonY = 400;
  int coodinatesOfferButtonX = 250;
  int coodinatesOfferButtonY = 300;
  int coodinatesPollButtonX = 250;
  int coodinatesPollButtonY = 350;
  int coodinatesPeekButtonX = 250;
  int coodinatesPeekButtonY = 400;
  int coodinatesClearButtonX = 50;
  int coodinatesClearButtonY = 500;
  void drawActionButtons(){
    textSize(lettersSize-5);
    fill(0);
    text("add",coodinatesAddButtonX, coodinatesAddButtonY);
    text("remove",coodinatesRemoveButtonX, coodinatesRemoveButtonY);
    text("element",coodinatesElementButtonX, coodinatesElementButtonY);
    text("offer",coodinatesOfferButtonX, coodinatesOfferButtonY);
    text("poll",coodinatesPollButtonX, coodinatesPollButtonY);
    text("peek",coodinatesPeekButtonX, coodinatesPeekButtonY);
    text("clear",coodinatesClearButtonX, coodinatesClearButtonY);
  }
  void performAction(String action, String result){
      actionReturn = result;
      switch(action){
      case "add":
      addQueue();
      break;
      case "remove":
      removeQueue();
      break;
      case "element":
      break;
      case "offer":
      break;
      case "peek":
      break;
      case "poll":
      break;
      case "clear":
      break;
    }
  }
  void addQueue(){
    
  }
  void removeQueue(){
  }
  
  /*void mousePressed(int x, int y) {
    String buttonPressed = getButtonPressed(x,y);
     switch(buttonPressed){
      case "add":
        controllerView.performedMethod("add");
      break;
      case "remove":
        controllerView.performedMethod("remove");
      break;
      case "element":
        controllerView.performedMethod("element");
      break;
     case "offer":
       controllerView.performedMethod("offer");
      break;
      case "poll":
        controllerView.performedMethod("poll");
      break;
      case "peek":
        controllerView.performedMethod("peek");
      break;
      case "clear":
        controllerView.performedMethod("clear");
      break;
    }    
  } */
  String getButtonPressed(int x,int y){
    actionReturn = "";
    if(dist(coodinatesAddButtonX,coodinatesAddButtonY,x,y)<30){
      return "add";
    }
    if(dist(coodinatesRemoveButtonX,coodinatesRemoveButtonY,x,y)<30 ){
      return "remove";
    }
    if(dist(coodinatesElementButtonX,coodinatesElementButtonY,x,y)<30 ){
      return "element";
    }
    if(dist(coodinatesOfferButtonX,coodinatesOfferButtonY,x,y)<30 ){
      return "offer";
    }
    if(dist(coodinatesPollButtonX,coodinatesPollButtonY,x,y)<30 ){
      return "poll";
    }
    if(dist(coodinatesPeekButtonX,coodinatesPeekButtonY,x,y)<30 ){
      return "peek";
    }
    if(dist(coodinatesClearButtonX,coodinatesClearButtonY,x,y)<30 ){
      return "clear";
    }
    return "";
  }

  
}