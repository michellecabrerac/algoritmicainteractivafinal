class ViewStructuresList implements ViewDataStructures{
 // ControllerView controllerView;
  String actionReturn;
  int sizeQueue;
  String classType;
  String [] idObjects;
   void setIdsObjects(String [] idObjects){
     this.idObjects = idObjects;
   }
  void setSize(int sizeQueue){
    this.sizeQueue = sizeQueue;
  }
  int getSize(){
  return this.sizeQueue;
  }
 void setClassType(String classType){
   this.classType = classType;
 }
  public ViewStructuresList(){
    // this.controllerView = new ControllerView();
     this.sizeQueue = 0;
     this.actionReturn = "";
     this.classType = "";
  }
 /* void setViewController(ControllerView controllerView){
    this.controllerView = controllerView;
  }*/
  void drawStructure(){
    drawObjects();
    drawSpace();
    drawActionButtons();
    drawActionReturn();
    drawClassType();
  }
  void drawClassType(){
    fill(0, 255, 153);
    text(classType,width-300,30);
  }
  void drawObjects(){
    fill(0, 102, 153);
    for(int i= 0; i<getSize(); i++){
      int startXRect = (100+ 55*i)+50*i;
      rect(startXRect, 100, 55, 55);
      //Partición list
      line(startXRect+25, 100, startXRect+25, 155);
      //Flechas
      float startX = startXRect + 55;
      float endX = startX + 50;
      line(startX, 125, endX, 125);
      //Para identificar al objeto de la lista
      textSize(15);
      text(idObjects[i],startXRect,180+(20*i)); // para que el id aparezca al lado del la representación del objeto
      text(""+(i), startXRect, 50 );
    }
  }
  void drawSpace(){

  }
  void drawActionReturn(){
    int coordinatesReturnResultX = width-350;
    int coordinatesReturnResultY = height - 500;
    noFill();
    rect(coordinatesReturnResultX, coordinatesReturnResultY, 350,280, 7);
    text("Result: ",coordinatesReturnResultX,coordinatesReturnResultY);
    fill(255,0,0);
    text(actionReturn,coordinatesReturnResultX+30,coordinatesReturnResultY+100);
  }
  int coodinatesAddButtonX = 50;
  int coodinatesAddButtonY = 300;
  int coodinatesClearButtonX = 50;
  int coodinatesClearButtonY = 500;
  int coodinatesIsEmptyButtonX = 50;
  int coodinatesIsEmptyButtonY = 350;
  int coodinatesIteratorButtonX = 50;
  int coodinatesIteratorButtonY = 400;
  int coodinatesListIteratorButtonX = 250;
  int coodinatesListIteratorButtonY = 300;
  int coodinatesRemoveButtonX = 250;
  int coodinatesRemoveButtonY = 350;
  int coodinatesToArrayButtonX = 250;
  int coodinatesToArrayButtonY = 400;

  void drawActionButtons(){
    textSize(lettersSize-5);
    fill(0);
    text("add",coodinatesAddButtonX, coodinatesAddButtonY);
    text("clear",coodinatesClearButtonX, coodinatesClearButtonY);
    text("isEmpty",coodinatesIsEmptyButtonX, coodinatesIsEmptyButtonY);
    text("iterator",coodinatesIteratorButtonX, coodinatesIteratorButtonY);
    text("listIterator",coodinatesListIteratorButtonX, coodinatesListIteratorButtonY);
    text("remove",coodinatesRemoveButtonX, coodinatesRemoveButtonY);
    text("toArray",coodinatesToArrayButtonX, coodinatesToArrayButtonY);

  }
  void performAction(String action, String result){
      actionReturn = result;
      switch(action){
      case "add":
      addQueue();
      break;
      case "clear":
      removeQueue();
      break;
      case "isEmpty":
      break;
      case "iterator":
      break;
      case "remove":
      break;
      case "toArray":
      break;
    }
  }
  void addQueue(){
    
  }
  void removeQueue(){
  }
  
  /*void mousePressed(int x, int y) {
    String buttonPressed = getButtonPressed(x,y);
     switch(buttonPressed){
      case "add":
        controllerView.performedMethod("add");
      break;
      case "clear":
        controllerView.performedMethod("clear");
      break;
      case "isEmpty":
        controllerView.performedMethod("isEmpty");
      break;
     case "iterator":
       controllerView.performedMethod("iterator");
      break;
      case "listIterator":
        controllerView.performedMethod("listIterator");
      break;
      case "remove":
        controllerView.performedMethod("remove");
      break;
      case "toArray":
        controllerView.performedMethod("toArray");
      break;
    }    
  }*/
  String getButtonPressed(int x,int y){
    actionReturn = "";
    if(dist(coodinatesAddButtonX,coodinatesAddButtonY,x,y)<30){
      return "add";
    }
    if(dist(coodinatesClearButtonX,coodinatesClearButtonY,x,y)<30 ){
      return "clear";
    }
    if(dist(coodinatesIsEmptyButtonX,coodinatesIsEmptyButtonY,x,y)<30 ){
      return "isEmpty";
    }
    if(dist(coodinatesIteratorButtonX,coodinatesIteratorButtonY,x,y)<30 ){
      return "iterator";
    }
    if(dist(coodinatesListIteratorButtonX,coodinatesListIteratorButtonY,x,y)<30 ){
      return "listIterator";
    }
    if(dist(coodinatesRemoveButtonX,coodinatesRemoveButtonY,x,y)<30 ){
      return "remove";
    }
    if(dist(coodinatesToArrayButtonX,coodinatesToArrayButtonY,x,y)<30 ){
      return "toArray";
    }

    return "";
  }

}