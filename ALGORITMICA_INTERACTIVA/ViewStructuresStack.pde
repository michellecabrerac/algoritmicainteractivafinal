class ViewStructuresStack implements ViewDataStructures{
   //ControllerView controllerView;
   /*PAra dibujar las animaciones*/
   String [] idObjects;
   void setIdsObjects(String [] idObjects){
     this.idObjects = idObjects;
   }
   /**/
  String actionReturn;
  int size;
  String classType;
  void setSize(int size){
    this.size = size; //size stack
  }
  int getSize(){
  return this.size;
  }
 void setClassType(String classType){
   this.classType = classType;
 }
  public ViewStructuresStack(){
     //this.controllerView = new ControllerView();
     this.size = 0;
     this.actionReturn = "";
     this.classType = "";
     idObjects = new String[0];
  }
  /*void setViewController(ControllerView controllerView){
    this.controllerView = controllerView;
  }*/
  void drawStructure(){
    drawObjects();
    drawSpace();
    drawActionButtons();
    drawActionReturn();
    drawClassType();
  }
  void drawClassType(){
    fill(0, 255, 153);
    text(classType,width-300,30);
  }
  void drawObjects(){
    if(idObjects != null){
      int coordinatesObjectX = 200;
      int coordinatesObjectY = 200;
      for(int i= 0; i<idObjects.length; i++){
      fill(0, 102, 153);
      //ellipse(100+ 55*i, 100, 55, 55);
      text(""+(i+1), coordinatesObjectX-10, coordinatesObjectY-i*50);
      rect(coordinatesObjectX  , coordinatesObjectY-i*50, 100,50, 7);
      fill(0);
      textSize(16);
      text(idObjects[i], coordinatesObjectX + 120, coordinatesObjectY-i*50); // para que el id aparezca al lado del la representación del objeto
    }
   }
  }
  void drawSpace(){

  }
  void drawActionReturn(){
    int coordinatesReturnResultX = width-350;
    int coordinatesReturnResultY = height - 500;
    noFill();
    rect(coordinatesReturnResultX, coordinatesReturnResultY, 350,280, 7);
    text("Result: ",coordinatesReturnResultX,coordinatesReturnResultY);
    fill(255,0,0);
    text(actionReturn,coordinatesReturnResultX+30,coordinatesReturnResultY+100);
  }
  int coodinatesEmptyButtonX = 50;
  int coodinatesEmptyButtonY = 300;
  int coodinatesPeekButtonX = 50;
  int coodinatesPeekButtonY = 350;
  int coodinatesPopButtonX = 50;
  int coodinatesPopButtonY = 400;
  int coodinatesPushButtonX = 250;
  int coodinatesPushButtonY = 300;
  int coodinatesSearchButtonX = 250;
  int coodinatesSearchButtonY = 350;
  
  void drawActionButtons(){
    textSize(lettersSize-5);
    fill(0);
    text("empty",coodinatesEmptyButtonX, coodinatesEmptyButtonY);
    text("peek",coodinatesPeekButtonX, coodinatesPeekButtonY);
    text("pop",coodinatesPopButtonX, coodinatesPopButtonY);
    text("push",coodinatesPushButtonX, coodinatesPushButtonY);
    text("search",coodinatesSearchButtonX, coodinatesSearchButtonY);
  }
  void performAction(String action, String result){
      actionReturn = result;
      switch(action){
      case "empty":
      break;
      case "peek":
      break;
      case "pop":
      break;
      case "push":
      break;
      case "search":
      break;
    }
  }
  
  /*void mousePressed(int x, int y) {
    String buttonPressed = getButtonPressed(x,y);
     switch(buttonPressed){
      case "empty":
        controllerView.performedMethod("empty");
      break;
      case "peek":
        controllerView.performedMethod("peek");
      break;
      case "pop":
        controllerView.performedMethod("pop");
      break;
     case "push":
       controllerView.performedMethod("push");
      break;
      case "search":
        controllerView.performedMethod("search");
      break;
    }    
  }*/
  String getButtonPressed(int x,int y){
    actionReturn = "";
    if(dist(coodinatesEmptyButtonX,coodinatesEmptyButtonY,x,y)<30){
      return "empty";
    }
    if(dist(coodinatesPeekButtonX,coodinatesPeekButtonY,x,y)<30 ){
      return "peek";
    }
    if(dist(coodinatesPopButtonX,coodinatesPopButtonY,x,y)<30 ){
      return "pop";
    }
    if(dist(coodinatesPushButtonX,coodinatesPushButtonY,x,y)<30 ){
      return "push";
    }
    if(dist(coodinatesSearchButtonX,coodinatesSearchButtonY,x,y)<30 ){
      return "search";
    }
    return "";
  }
}