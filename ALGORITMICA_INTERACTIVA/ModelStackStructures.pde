class ModelStackStructures implements ModelDataStructures{
  
  Stack stack;
  String typeOfStack;
  public ModelStackStructures(String type){
    setTypeOfStack(type);
  }
  private void setTypeOfStack(String type){
    String classType = type.toUpperCase();
      switch(classType){
      case "STACK":
        typeOfStack = "Stack";
        stack =  new Stack();
      break;
      default: 
      typeOfStack = "Stack";
      stack =  new Stack();
    }
  }
  public String getClassType(){
    return this.typeOfStack;
  }
  String performAction(String action){
    String result = "";
      switch(action){
      case "empty":
      result = result + stack.empty();
      break;
      case "peek":
       result = result + stack.peek();
      break;
      case "pop":
       result = result + stack.pop();
      break;
      case "push":
        result = result + stack.push(new Object());
      break;
      case "search":
       result = result + stack.search(new Object());
      break;
    }
    return result;
  }
  int getSize(){
    return stack.size();
  }
  String[] getObjectsIDs(){
    Object [] array = stack.toArray();
    String [] ids = new String [array.length];
    for(int i = 0; i<array.length; i++){
      ids[i] = array[i].toString();
    }
    return ids;
  }
}