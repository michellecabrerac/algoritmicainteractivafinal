/*Code Spy*/
Lo primero que debes de hacer para utilizar CodeSpy es:
1 - Entrar a la pantalla CodeSpy a través del menú
2 - Insertar el código Java en el editor debajo de su token correspondiente
    (imports, atributos, el contenido del main y los métodos utilizados)
3- Guardar y compilar el código introducido
4- Pulsar el botón next

¡Ya puedes comenzar a espiar tu código!


/*Estructuras de datos*/
Lo primero que debes de hacer para utilizar Estructuras de datos es:
1 - Entrar a la pantalla Estructuras a través del menú
2- Elegir entre las estructuras disponibles
3- Pulsar las acciones para visualizar como trabaja esa estructura
