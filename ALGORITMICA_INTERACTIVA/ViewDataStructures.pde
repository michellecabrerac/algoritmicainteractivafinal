interface ViewDataStructures {
 // void setViewController(ControllerView viewController){}
  void drawStructure();
  //void mousePressed(int x, int y);
  void performAction(String action, String result);
  void setSize(int size); //<>//
  int getSize();
  void setClassType(String classType);
  void setIdsObjects(String [] ids);
  String getButtonPressed(int x, int y);
}